import os
from subprocess import call

os.makedirs("web",exist_ok=True)

for path in os.listdir("raw"):
	call("ffmpeg -y -i raw/{} -vf scale=640:360 -crf 20 web/{}".format(path,path), shell=True)
